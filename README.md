# A collection of `Dockerfile` templates

This is GitLabs collection of `Dockerfiles` to be used in conjunction with
[GitLab CI][gl-ci]. These files are used to populate the `Dockerfile` template
choosers available on all GitLab instances running 9.2 or later.

For more information about how the files work, and how to create them,
please read the [documentation on Dockerfiles][docker-docs]. Please keep in mind that
these templates might need editing to suit your setup, and should be considered
guideline.

## Contributing guidelines

Please see the [contribution guidelines](CONTRIBUTING.md)

[gl-ci]: https://about.gitlab.com/gitlab-ci/
[docker-docs]: https://docs.docker.com/engine/reference/builder/
